package com.chatbotize.plugins.survey.infrastructure.kafka

import java.util.Properties

import akka.Done
import com.chatbotize.plugin.response.Response
import org.apache.kafka.clients.producer.{Callback, KafkaProducer, ProducerRecord, RecordMetadata}

import scala.concurrent.{Future, Promise}

class KafkaResponseService(topic: String, bootstrapServers: String) {

  private val properties = {
    val props = new Properties()
    props.put("bootstrap.servers", bootstrapServers)
    props.put("acks", "all")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
    props
  }

  private def getKey(response: Response): String = s"${response.platformId}-${response.chatbotId}-${response.userId}"

  private val producer = new KafkaProducer[String, Array[Byte]](properties)

  def respond(response: Response): Future[Done] = {
    val record = new ProducerRecord[String, Array[Byte]](
      topic,
      getKey(response),
      response.toByteArray
    )

    val promise = Promise[Done]

    producer.send(
      record,
      new Callback {
        override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit =
          if (exception != null) {
            promise.failure(exception)
          } else {
            promise.success(Done)
          }
      }
    )

    promise.future
  }
}
