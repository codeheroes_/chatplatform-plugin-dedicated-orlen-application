package com.chatbotize.plugins.survey.domain.actor

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

import akka.Done
import akka.actor.{Actor, Stash, Status, Timers}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.chatbotize.plugin.request.Request
import com.chatbotize.plugin.request.Request.Action.{Abort, Empty, Initialize, Message}
import com.chatbotize.plugin.response._
import com.chatbotize.plugins.survey.domain.Defs
import com.chatbotize.plugins.survey.domain.Defs.{ChatbotId, PlatformId, UserId}
import com.chatbotize.plugins.survey.domain.actor.SurveyActor._
import com.chatbotize.plugins.survey.infrastructure.kafka.KafkaResponseService
import com.chatbotize.protocol.request.Message.Payload
import com.chatbotize.protocol.request.{Message => RequestMessage}
import com.chatbotize.protocol.response
import com.chatbotize.protocol.response.Message.{Payload => RPayload}
import com.chatbotize.protocol.response.{MessageTag, Message => ResponseMessage}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

class SurveyActor(platformId: PlatformId, chatbotId: ChatbotId, userId: UserId)(
    responseService: KafkaResponseService
)(implicit ec: ExecutionContext, mat: ActorMaterializer)
    extends Actor
    with StrictLogging
    with Stash
    with Timers {

  private val format = DateTimeFormatter.ofPattern("dd.MM.yyyy")

  private def getDate(value: String): Option[LocalDate] =
    try {
      Some(LocalDate.parse(value, format))
    } catch {
      case _: Throwable => None
    }

  override def receive: Receive = handling

  private def handling: Receive = {
    case action: Request.Action =>
      sender() ! Done
      action match {
        case Initialize(_)  => context.become(collecting(Steps.GetStartDate))
        case Message(value) => unexpected(value.message)
        case Empty          => finish()
        case Abort(_)       => abort()
      }

    case other => handleOther(other, "handling")

  }

  private def collecting(step: Step): Receive = {
    step match {
      case Steps.GetStartDate =>
        respondWith(Seq(RPayload.Text(response.Text("No dobrze, kiedy chcesz rozpocząć urlop?"))))

      case Steps.GetEndDate(_) =>
        respondWith(Seq(RPayload.Text(response.Text("Mam to!, Kiedy chcesz zakończyć urlop?"))))

      case Steps.GetType(_, _) =>
        respondWith(
          Seq(
            RPayload.QuickButtons(response.QuickButtons(
              "Jasne! Czy ma to być urlop płatny czy bezpłatny?",
              Seq(
                response.QuickButton("Płatny", Defs.BUTTON_PAID_YES),
                response.QuickButton("Bezpłatny", Defs.BUTTON_PAID_NO),
              )
            ))))

      case Steps.Confirm(startDate, endDate, tpe) =>
        val tpeStr = tpe match {
          case Types.Paid => "płatny"
          case Types.Free => "bezpłatny"
        }
        respondWith(
          Seq(
            RPayload.QuickButtons(response.QuickButtons(
              s"Czy chcesz złożyć wniosek na $tpeStr urlop (${ChronoUnit.DAYS
                .between(startDate, endDate)} dni) od ${format.format(startDate)} do ${format.format(endDate)}",
              Seq(
                response.QuickButton("Tak", Defs.CONFIRM_YES),
                response.QuickButton("Nie", Defs.CONFIRM_NO),
              )
            ))))

    }

    {
      case action: Request.Action =>
        sender() ! Done
        action match {
          case Initialize(_) => context.become(collecting(Steps.GetStartDate))

          case Message(value) =>
            value.message.payload match {

              case Payload.Text(txt) =>
                step match {
                  case Steps.GetStartDate =>
                    getDate(txt.value) match {
                      case Some(date) => context.become(collecting(Steps.GetEndDate(date)))
                      case None       => context.become(collecting(step))
                    }

                  case Steps.GetEndDate(startDate) =>
                    getDate(txt.value) match {
                      case Some(date) => context.become(collecting(Steps.GetType(startDate, date)))
                      case None       => context.become(collecting(step))
                    }

                  case Steps.GetType(startDate, endDate) =>
                    if (Set("bezpłatny", "bezplatny", "darmowy").contains(txt.value.toLowerCase())) {
                      context.become(collecting(Steps.Confirm(startDate, endDate, Types.Free)))
                    } else if (Set("płatny", "platny").contains(txt.value.toLowerCase())) {
                      context.become(collecting(Steps.Confirm(startDate, endDate, Types.Paid)))
                    } else {
                      context.become(collecting(step))
                    }

                  case Steps.Confirm(_, _, _) => context.become(collecting(step))
                }

              case Payload.Button(btn) =>
                btn.buttonId match {
                  case Defs.BUTTON_PAID_YES =>
                    step match {
                      case Steps.GetType(startDate, endDate) =>
                        context.become(collecting(Steps.Confirm(startDate, endDate, Types.Paid)))
                      case _ =>
                        context.become(collecting(step))
                    }

                  case Defs.BUTTON_PAID_NO =>
                    step match {
                      case Steps.GetType(startDate, endDate) =>
                        context.become(collecting(Steps.Confirm(startDate, endDate, Types.Free)))
                      case _ =>
                        context.become(collecting(step))
                    }

                  case Defs.CONFIRM_YES =>
                    step match {
                      case Steps.Confirm(startDate, endDate, tpe) =>
                        respondWithAndFinish(
                          Seq(
                            RPayload.Text(response.Text("Twój wniosek został złożony! Powiadomimy Cię o jego wyniku"))))
                        timers.startSingleTimer(NotifyKey, Notify(startDate, endDate, tpe), 15 seconds)

                      case _ =>
                        context.become(collecting(step))
                    }

                  case Defs.CONFIRM_NO =>
                    step match {
                      case Steps.Confirm(_, _, _) =>
                        respondWithAndFinish(Seq(RPayload.Text(response.Text("Rozumiem, pracuj dzielnie!"))))

                      case _ =>
                        context.become(collecting(step))
                    }

                  case _ => context.become(collecting(step))

                }
              case _ => context.become(collecting(step))
            }
          case Empty    => finish()
          case Abort(_) => abort()
        }

      case other => handleOther(other, "handling")

    }
  }

  private def logError(message: String, ex: Throwable): Unit =
    logger.error(s"$message for platformId: $platformId, chatbotId: $chatbotId, userId: $userId", ex)

  private def logWarn(message: String): Unit =
    logger.warn(s"$message for platformId: $platformId, chatbotId: $chatbotId, userId: $userId")

  private def respond(actions: Response.Action*): Unit =
    Source(actions.toList)
      .mapAsync(1)(
        action =>
          responseService
            .respond(
              Response(
                pluginId = Defs.PLUGIN_ID,
                instanceId = Defs.INSTANCE_ID,
                platformId = platformId,
                chatbotId = chatbotId,
                userId = userId,
                action = action
              ))
      )
      .runWith(Sink.ignore)
      .onComplete {
        case Success(_)  => //ignore
        case Failure(ex) => logError("Error while sending response", ex)
      }

  private def finish(): Unit = {
    respond(Response.Action.LoudFinish(ActionLoudFinish()))
    context.stop(self)
  }

  private def unexpected(message: RequestMessage): Unit =
    respond(Response.Action.Unexpected(ActionUnexpected(message)))

  private def respondWith(payloads: Seq[RPayload]): Unit =
    respond(Response.Action.Response(ActionResponse(payloads.map(p => ResponseMessage(MessageTag.DEFAULT, p)))))

  private def respondWithAndFinish(payload: Seq[RPayload]): Unit = {
    respond(
      Response.Action.Response(ActionResponse(payload.map(p => ResponseMessage(MessageTag.DEFAULT, p)))),
      Response.Action.LoudFinish(ActionLoudFinish()))

  }

  private def handleOther(other: Any, state: String): Unit = {
    other match {

      case Notify(startDate, endDate, tpe) =>
        val tpeStr = tpe match {
          case Types.Paid => "płatny"
          case Types.Free => "bezpłatny"
        }
        respondWith(
          Seq(
            RPayload.Text(response.Text(
              s"Doskonała wiadmomość! Twój wniosek o $tpeStr urlop (${ChronoUnit.DAYS
                .between(startDate, endDate)} dni) od ${format.format(startDate)} do ${format.format(endDate)} został pozytywnie rozpatrzony!",
            )),
            RPayload.Text(response.Text("Miłego wypoczynku!"))
          ))

      case _ =>
        logWarn(s"Unexpected $other command in $state state")
        if (sender() != self) {
          sender() ! Status.Failure(new IllegalStateException(s"Unexpected $other command in initialized state"))
        }
    }
  }

  private def abort(): Unit = context.stop(self)

}

object SurveyActor {
  sealed trait Step

  object Steps {
    final case object GetStartDate                                                extends Step
    final case class GetEndDate(startDate: LocalDate)                             extends Step
    final case class GetType(startDate: LocalDate, endDate: LocalDate)            extends Step
    final case class Confirm(startDate: LocalDate, endDate: LocalDate, tpe: Type) extends Step
  }

  sealed trait Type
  object Types {
    final case object Paid extends Type
    final case object Free extends Type
  }

  final case object NotifyKey
  final case class Notify(startDate: LocalDate, endDate: LocalDate, tpe: Type)
}
