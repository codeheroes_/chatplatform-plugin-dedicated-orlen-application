package com.chatbotize.plugins.survey.domain

object Defs {

  type InstanceId = String
  type UserId     = String
  type PlatformId = String
  type ChatbotId  = String

  val PLUGIN_ID   = "dedicated-orlen-application"
  val INSTANCE_ID = "default"

  val BUTTON_PAID_YES = "BUTTON_PAID_YES"
  val BUTTON_PAID_NO  = "BUTTON_PAID_NO"

  val CONFIRM_YES = "CONFIRM_YES"
  val CONFIRM_NO  = "CONFIRM_NO"
}
