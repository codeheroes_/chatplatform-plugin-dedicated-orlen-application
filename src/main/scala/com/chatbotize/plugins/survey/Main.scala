package com.chatbotize.plugins.survey

import com.chatbotize.plugins.survey.Application.{KafkaConfig, ServicesConfig}
import com.typesafe.config.ConfigFactory

object Main extends App {

  private val config = ConfigFactory.load("default.conf")
  private val services = ServicesConfig(
    KafkaConfig(config.getString("kafka.host"), config.getInt("kafka.port"))
  )
  private val application = new Application(config, services)

  application.start()
}
